# GitLab Pages Site

<!-- Badges -->
[![license](https://img.shields.io/badge/license-MIT-blue)](https://gitlab.com/Feakster/website/-/blob/master/LICENSE.md)
[![Mozilla HTTP Observatory Grade](https://img.shields.io/mozilla-observatory/grade/feakster.io?publish)](https://observatory.mozilla.org/analyze/feakster.io)
[![Website](https://img.shields.io/website?url=https%3A%2F%2Ffeakster.io)](https://feakster.io)

## Description
I have no idea what to do with this at the moment. For now, it's just a template.

## Viewing
I'm still in the process of sorting out teething domain name issues. Until then, you can view the website at: https://feakster.gitlab.io/website or https://feakster.io.

## Known Issues
See [Issues](https://gitlab.com/Feakster/website/-/issues) on the GitLab repository.
